export const environment = {
  production: false,
  api: "https://app.exitum.us:9080/api",
  documents: "https://app.exitum.us:9080/documentos/",
};
