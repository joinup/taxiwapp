import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { AuthProvider } from '../auth/auth';


import { environment } from '../../environments/environment';

import * as moment from 'moment';

/*
  Generated class for the ServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ServiceProvider {

  currentUser: string;

  constructor(public http: HttpClient, public auth: AuthProvider) {
    this.currentUser = localStorage.getItem('idUsuario');
  }

  public getLang(){
    return localStorage.getItem('lang')=='es'?'esp':"ing";
  }

  public get(id: number) {
    const data = {
      "idViaje": id,
      "idUsuario": this.currentUser,
      "Idioma": this.getLang(),
    }
    return this.http.post(`${environment.api}/st_viajes_consulta`, data);
  }

  public getStatus(id: number): Observable<any>{
    const data = {
      "idViaje": id,
      "idUsuario": this.currentUser,
      "Idioma": this.getLang(),
    }
    return this.http.post(`${environment.api}/st_estatusViaje_consulta`, data);
  }

  public getLast(): Observable<any>{
    const data = {
      "idUsuario": this.currentUser,
      "Idioma": this.getLang(),
    }
    return this.http.post(`${environment.api}/st_viajes_Consulta_10`, data);
  }

  public getTypeServices(): Observable<any>{
    const data = {
      "idUsuario": this.currentUser,
      "Idioma": this.getLang(),
    }
    return this.http.post(`${environment.api}/st_tipoServicios_consulta`, data);
  }

  public addService(service: Object): Observable<any>{
    const data = {
      ...service,
      "idUsuario": this.currentUser,
      "Idioma": this.getLang(),
    }
    return this.http.post(`${environment.api}/st_viajes_insertar`, data);
  }

  public getDrivers(idTypeService, cord): Observable<any>{
    const data = {
      "idtipoServicio": idTypeService,
      "idUsuario": this.currentUser,
      "posicion": cord,
      "Idioma": this.getLang(),
    }
    return this.http.post(`${environment.api}/st_conductoresCerca_consulta`, data);
  }

  addFavorite(info): Observable<any>{
    const data = {
      "idUbicacionFavorita": 1,
      "Descripcion": info.Descripcion,
      "puntoGeografico": info.puntoGeografico,
      "Direccion": info.Direccion,
      "update_idu": this.currentUser,
      "update_date": moment().format('YYYY-MM-DD'),
      "idUsuario": this.currentUser,
      "Idioma": this.getLang(),
    }
    return this.http.post(`${environment.api}/st_usuariosUbicacionesFavoritas_insertar`, data);
  }

  public getFavorites(): Observable<any>{
    const data = {
      "idUsuario": this.currentUser,
      "Idioma": this.getLang(),
    }
    return this.http.post(`${environment.api}/st_usuariosUbicacionesFavoritas_consultar`, data);
  }

  public getMotiveCancel(): Observable<any> {
    const data = {
      "esParaPasajero": 1,
      "idUsuario": this.currentUser,
      "Idioma": this.getLang(),
    }
    return this.http.post(`${environment.api}/st_motivosDeCancelaciones_consulta`, data);
  }

  public addMotiveCancel(id: number, motive: number): Observable<any>{
    const data = {
      "idViaje": id,
      "idMotivoCancelacion": motive,
      "quienCancela": 1,
      "idUsuario": this.currentUser,
      "Idioma": this.getLang(),
    }
    return this.http.post(`${environment.api}/st_viajeCancelaciones_insertar`, data);
  }

  public setSegurity({id, compartirMiViaje, avisarSiSOS, solicitarmePin, enviarSMSpinINcorrecto}): Observable<any>{
    const data = {
      "idViaje": id,
      "compartirMiViaje": compartirMiViaje,
      "avisarSiSOS": avisarSiSOS,
      "solicitarmePin": solicitarmePin,
      "enviarSMSpinINcorrecto": enviarSMSpinINcorrecto,
      "idUsuario": this.currentUser,
      "Idioma": this.getLang(),
    }
    return this.http.post(`${environment.api}/st_viajeConfiguracionSeguridad_insertar`, data);
  }

  public addDriverService({id, idConductor, idVehiculo}): Observable<any>{
    const data = {
      "idViaje": id,
      "idConductor": idConductor,
      "idVehiculo": idVehiculo,
      "idUsuario": this.currentUser,
      "Idioma": this.getLang(),
    }
    return this.http.post(`${environment.api}/st_viajeConductorConfirmado_inserta`, data);
  }

  public initService({id, puntopartida}): Observable<any>{
    const data = {
      "idViaje": id,
      "puntopartida": puntopartida,
      "idUsuario": this.currentUser,
      "Idioma": this.getLang(),
    }
    return this.http.post(`${environment.api}/st_viajeIniciado_insertar`, data);
  }

  getPin({id, pin}): Observable<any>{
     const data = {
      "idViaje": id,
      "PIN": pin,
      "Idioma": this.getLang(),
    }
    return this.http.post(`${environment.api}/st_viajesVeficarPINSeguridad_consulta`, data);
  }


  cancelDriverTravel(id): Observable<any>{
    const data = {
      "idViaje": id,
      "idStatus": 7,
      "idUsuario": this.currentUser,
      "Idioma": this.getLang(),
    }
    return this.http.post(`${environment.api}/st_viajeCondConfirm_actualiza`, data);
  }

  getTaximetro(id): Observable<any>{
    const data = {
      "idViaje": id,
      "Idioma": this.getLang(),
    }
    return this.http.post(`${environment.api}/st_viajeTaximetro_consulta`, data);
  }

  getLastPosition(id): Observable<any>{
    const data = {
      "idViaje": id,
      "Idioma": this.getLang(),
    }
    return this.http.post(`${environment.api}/st_conductoresUltimaPosicion_consulta`, data);
  }

  finishService({id, propine, point}): Observable<any>{
    const data = {
      "idViaje": id,
      "puntoLlegada": point,
      "PorcentajePropina": propine,
      "idUsuario": this.currentUser,
      "Idioma": this.getLang(),
    }
    return this.http.post(`${environment.api}/st_viajeFinalizacion_insertar`, data);
  }

  getLastServices(): Observable<any>{
    const data = {
      "idUsuario": this.currentUser,
      "Idioma": this.getLang(),
    }
    return this.http.post(`${environment.api}/st_viajes_ConcluidosConsulta`, data);
  }

  getProgrames(): Observable<any>{
    const data = {
      "idUsuario": this.currentUser,
      "Idioma": this.getLang(),
    }
    return this.http.post(`${environment.api}/st_viajes_ProgramadosConsulta`, data);
  }
}
