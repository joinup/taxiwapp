export class Service{
  id: any;
  idTipoServicio: number;
  puntoOrigen: string;
  puntoDestino: string;
  fechaHoraSolicitud: string;
  idEstatusViaje: number = 0;
  costo: number;
}